# Notepad++ Text Editor for Android Mobile

Notepad+ text editor is a notepad for open and edit any type of text file like txt, html, xml, js, php, css, asp, cpp, c etc.

Features:
Open any type of file to edit.
Share file on mail and any other platform direct.
Listen file text.
View all notepad files on single place.

Download from Playstore

https://play.google.com/store/apps/details?id=com.smartappspro.notepad
